/*
 * Copyright (c) Jonas Aaberg <cja@lithops.se> 2019
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/quotedprintable"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/OpenPeeDeeP/xdg"
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message"
	"github.com/emersion/go-message/mail"

	netmail "net/mail"

	"github.com/gorilla/feeds"
	"golang.org/x/net/html/charset"
	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/encoding/unicode"
)

const DEFAULT_SERVER = "imap.server:993"
const CONFIG_FILE = "config.json"

var xdgh = xdg.New("gmelchett", "imap2atom")

type maildata struct {
	from    string
	date    time.Time
	subject string
	html    []byte
	text    []byte
}

type Config struct {
	ImapServer     string `json:"imapserver"`
	Username       string `json:"username"`
	Password       string `json:"password"`
	Title          string `json:"title"`
	OutputFileName string `json:"outputfilename"`
	Link           string `json:"link"`
	MailBox        string `json:"mailbox"`
}

func createDir(dir string) (err error) {

	if stat, err := os.Stat(dir); err != nil || !stat.IsDir() {
		err = os.MkdirAll(dir, 0755)
	}
	return
}

func saveFile(file string, d []byte) {
	if f, err := os.OpenFile(file, os.O_RDWR|os.O_CREATE, 0644); err == nil {
		f.Write(d)
		f.Close()
	} else {
		fmt.Println("Failed to save file:"+file, err)
	}
}

func jsonOpen(file *string, w interface{}) (err error) {

	f, err := os.Open(*file)
	if err != nil {
		return
	}
	defer f.Close()
	if err := json.NewDecoder(f).Decode(w); err != nil {
		fmt.Println("Failed decoding json", err)
	}
	return
}

func configLoad() *Config {

	if err := createDir(xdgh.ConfigHome()); err != nil {
		fmt.Println("Failed creating config directory", err)
		os.Exit(1)
	}

	config := new(Config)

	configFile := filepath.Join(xdgh.ConfigHome(), CONFIG_FILE)

	if err := jsonOpen(&configFile, config); err == nil {
		return config
	}

	configDefault := Config{
		ImapServer:     DEFAULT_SERVER,
		Title:          "Mailbox",
		OutputFileName: "mailbox.atom",
		MailBox:        "INBOX",
		Link:           "https://www.favicon.cc/?action=icon&file_id=33101"}

	if jsonConfig, err := json.MarshalIndent(configDefault, "", "    "); err == nil {
		saveFile(configFile, jsonConfig)
	}
	return &configDefault
}

func main() {

	config := configLoad()

	if config.ImapServer == DEFAULT_SERVER {
		fmt.Println("You need to configure imap2atom.")
		fmt.Println("Your configuration file: " + filepath.Join(xdgh.ConfigHome(), CONFIG_FILE))
		os.Exit(1)
	}

	mails := fetchMails(config.ImapServer, config.MailBox, config.Username, config.Password)

	myfeeds := &feeds.Feed{Title: config.Title,
		Link: &feeds.Link{Href: config.Link},
	}

	for i := range mails {
		saveAsFeed(myfeeds, mails[i])
	}

	if file, err := os.OpenFile(config.OutputFileName, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644); err == nil {
		myfeeds.WriteAtom(file)
		file.Close()
	} else {
		fmt.Println("Failed to save:"+config.OutputFileName, err)
	}
}

// from: https://gist.github.com/nl5887/8773c4fa20695a71c30c
func decoder(encoding string) (*encoding.Decoder, error) {
	if strings.ToUpper(encoding) == "UTF-8" {
		return unicode.UTF8.NewDecoder(), nil
	} else if strings.ToUpper(encoding) == "ISO-8859-1" {
		return charmap.ISO8859_1.NewDecoder(), nil
	} else {
		return nil, fmt.Errorf("Unknown encoding")
	}
}

func decodeHeader(str string) (string, error) {
	re := regexp.MustCompile(`\=\?(?P<charset>.*?)\?(?P<encoding>.*)\?(?P<body>.*?)\?(.*?)\=`)

	matches := re.FindAllStringSubmatch(str, -1)
	if len(matches) == 0 {
		return str, nil
	}

	for _, match := range matches {
		var r io.Reader = strings.NewReader(match[3])

		if match[2] == "Q" {
			r = quotedprintable.NewReader(r)
		} else if match[2] == "B" {
			r = base64.NewDecoder(base64.StdEncoding, r)
		}

		if d, err := decoder(match[1]); err == nil {
			r = d.Reader(r)
		}

		if val, err := ioutil.ReadAll(r); err == nil {
			str = strings.Replace(str, match[0], string(val), -1)
		} else if err != nil {
			fmt.Println(err.Error())
			continue
		}

	}

	return str, nil
}

func saveAsFeed(myfeeds *feeds.Feed, md *maildata) {

	title := md.subject

	if t, err := decodeHeader(md.subject); err == nil {
		title = t
	}

	if e, err := netmail.ParseAddress(md.from); err == nil {
		if from, err := decodeHeader(e.Name); err == nil {
			title = from + ": " + title
		}
	}
	item := feeds.Item{Created: md.date,
		Link:  &feeds.Link{Href: md.from},
		Title: title,
	}

	if len(md.html) > 0 {
		item.Content = string(md.html)
	}

	myfeeds.Add(&item)
}

func fetchMails(server, mailbox, username, password string) []*maildata {

	// Connect to server
	c, err := client.DialTLS(server, nil)
	if err != nil {
		fmt.Println("Failed to connect to server", err)
		os.Exit(1)
	}

	// Don't forget to logout
	defer c.Logout()

	// Login
	if err := c.Login(username, password); err != nil {
		fmt.Println("Failed to login", err)
		os.Exit(1)
	}

	// Select INBOX
	mbox, err := c.Select(mailbox, false)
	if err != nil {
		fmt.Println("Failed to select box:", mailbox, err)
		os.Exit(1)
	}

	message.CharsetReader = charset.NewReaderLabel

	// Get the last 20 messages
	from := uint32(1)
	to := mbox.Messages
	if mbox.Messages > 19 {
		// We're using unsigned integers here, only substract if the result is > 0
		from = mbox.Messages - 19
	}
	seqset := new(imap.SeqSet)
	seqset.AddRange(from, to)

	// Get the whole message body
	var section imap.BodySectionName
	items := []imap.FetchItem{section.FetchItem()}

	messages := make(chan *imap.Message, 100) // > 20

	if err := c.Fetch(seqset, items, messages); err != nil {
		fmt.Println("Failed to fetch messages", err)
		os.Exit(1)
	}

	mails := make([]*maildata, 0, 20)

	for msg := range messages {

		m := maildata{}

		if msg == nil {
			fmt.Println("Server didn't returned message")
			os.Exit(1)
		}

		r := msg.GetBody(&section)
		if r == nil {
			fmt.Println("Server didn't returned message body")
			os.Exit(1)
		}

		// Create a new mail reader
		mr, err := mail.CreateReader(r)
		if err != nil {
			fmt.Println("Failed to create reader", err)
			os.Exit(1)
		}

		// Print some info about the message
		header := mr.Header
		if date, err := header.Date(); err == nil {
			m.date = date
		}
		if from, err := header.AddressList("From"); err == nil {

			if len(from) > 0 {
				m.from = from[0].String()
			}
		}
		if subject, err := header.Subject(); err == nil {
			m.subject = subject
		}

		// Process each message's part
		for {
			p, err := mr.NextPart()
			if err != nil {
				break
			}
			switch p.Header.(type) {
			case *mail.InlineHeader:
				ct, _, _ := p.Header.(*mail.InlineHeader).ContentType()
				if ct == "text/plain" {
					m.text, _ = ioutil.ReadAll(p.Body)
				} else if ct == "text/html" {
					m.html, _ = ioutil.ReadAll(p.Body)
				}
			case *mail.AttachmentHeader:
			}
		}

		mails = append(mails, &m)

	}
	return mails
}
