module imap2atom

go 1.18

require (
	github.com/OpenPeeDeeP/xdg v1.0.0
	github.com/emersion/go-imap v1.2.1
	github.com/emersion/go-message v0.16.0
	github.com/gorilla/feeds v1.1.1
	golang.org/x/net v0.0.0-20220725212005-46097bf591d3
	golang.org/x/text v0.3.7
)

require (
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21 // indirect
	github.com/emersion/go-textwrapper v0.0.0-20200911093747-65d896831594 // indirect
	github.com/kr/pretty v0.3.0 // indirect
)
