# imap2atom - Converts mail from imap folder to RSS/Atom feed

I consume the internet via RSS/Atom feeds. I dislike getting so called "News letter"
in my e-mail. I rather keep email for personal correspondence and mailing lists.
So I wrote this little program to convert mails from an imap folder to an atom-feed.

## Installation
Installation requires the golang tool suite. Sorry, no ready made packages yet.

1. Clone the git
2. go get
3. go build
4. Move `imap2atom` somewhere convenient.

## Configuration
1. Run imap2atom once, it will generated a template configuration.
2. Edit the generated template configuration to fit your needs
3. Put the output file somewhere where your RSS reader can reach it.

For security reasons, I suggest that you use a dedicated mail account for imap2atom,
since the password will not be encrypted in any way on disk.

Also, imap2atom does not remove any old mails. You have to do that by yourself.

## Licence

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
